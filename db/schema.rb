# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160911081136) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "attributes", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.boolean  "allow_multiple"
    t.boolean  "required"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "business_business_statuses", force: :cascade do |t|
    t.integer  "business_id"
    t.integer  "business_status_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.index ["business_id"], name: "index_business_business_statuses_on_business_id", using: :btree
    t.index ["business_status_id"], name: "index_business_business_statuses_on_business_status_id", using: :btree
  end

  create_table "business_statuses", force: :cascade do |t|
    t.string   "name"
    t.boolean  "business_active"
    t.text     "description"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "businesses", force: :cascade do |t|
    t.string   "name"
    t.string   "username"
    t.text     "description"
    t.boolean  "active"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["username"], name: "index_businesses_on_username", using: :btree
  end

  create_table "customer_groups", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "fee_value_types", force: :cascade do |t|
    t.integer  "fee_id"
    t.integer  "value_type_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["fee_id"], name: "index_fee_value_types_on_fee_id", using: :btree
    t.index ["value_type_id"], name: "index_fee_value_types_on_value_type_id", using: :btree
  end

  create_table "fees", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.float    "value"
    t.boolean  "active"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "locations", force: :cascade do |t|
    t.string   "name"
    t.string   "country"
    t.string   "province"
    t.string   "city"
    t.string   "address_line_one"
    t.string   "address_line_two"
    t.string   "zip_code"
    t.string   "formatted_address"
    t.float    "lng"
    t.float    "lat"
    t.string   "owner_type"
    t.integer  "owner_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.index ["owner_type", "owner_id"], name: "index_locations_on_owner_type_and_owner_id", using: :btree
  end

  create_table "merchant_accounts", force: :cascade do |t|
    t.integer  "business_id"
    t.string   "currency_code"
    t.string   "country_code"
    t.string   "provider_user_id"
    t.string   "provider_publishable_key"
    t.string   "provider_secret_key"
    t.string   "provider_access_token"
    t.string   "provider_refresh_token"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.index ["business_id"], name: "index_merchant_accounts_on_business_id", using: :btree
  end

  create_table "order_item_product_attributes", force: :cascade do |t|
    t.integer  "order_item_id"
    t.integer  "product_attribute_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.index ["order_item_id"], name: "index_order_item_product_attributes_on_order_item_id", using: :btree
    t.index ["product_attribute_id"], name: "index_order_item_product_attributes_on_product_attribute_id", using: :btree
  end

  create_table "order_item_products", force: :cascade do |t|
    t.integer  "order_item_id"
    t.integer  "product_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["order_item_id"], name: "index_order_item_products_on_order_item_id", using: :btree
    t.index ["product_id"], name: "index_order_item_products_on_product_id", using: :btree
  end

  create_table "order_items", force: :cascade do |t|
    t.float    "sub_total"
    t.float    "fee_total"
    t.float    "total"
    t.integer  "quantity"
    t.text     "notes"
    t.integer  "order_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["order_id"], name: "index_order_items_on_order_id", using: :btree
  end

  create_table "order_order_statuses", force: :cascade do |t|
    t.integer  "order_id"
    t.integer  "order_status_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.index ["order_id"], name: "index_order_order_statuses_on_order_id", using: :btree
    t.index ["order_status_id"], name: "index_order_order_statuses_on_order_status_id", using: :btree
  end

  create_table "order_statuses", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "orders", force: :cascade do |t|
    t.float    "sub_total"
    t.float    "fee_total"
    t.float    "total"
    t.text     "notes"
    t.string   "currency_code"
    t.integer  "customer_group_id"
    t.integer  "business_id"
    t.integer  "merchant_account_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.index ["business_id"], name: "index_orders_on_business_id", using: :btree
    t.index ["customer_group_id"], name: "index_orders_on_customer_group_id", using: :btree
    t.index ["merchant_account_id"], name: "index_orders_on_merchant_account_id", using: :btree
  end

  create_table "phone_numbers", force: :cascade do |t|
    t.string   "name"
    t.string   "value"
    t.string   "owner_type"
    t.integer  "owner_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["owner_type", "owner_id"], name: "index_phone_numbers_on_owner_type_and_owner_id", using: :btree
  end

  create_table "product_attributes", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.float    "price"
    t.boolean  "active"
    t.integer  "quantity"
    t.integer  "product_id"
    t.integer  "attribute_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["attribute_id"], name: "index_product_attributes_on_attribute_id", using: :btree
    t.index ["product_id"], name: "index_product_attributes_on_product_id", using: :btree
  end

  create_table "product_fees", force: :cascade do |t|
    t.integer  "fee_id"
    t.integer  "product_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["fee_id"], name: "index_product_fees_on_fee_id", using: :btree
    t.index ["product_id"], name: "index_product_fees_on_product_id", using: :btree
  end

  create_table "product_group_fees", force: :cascade do |t|
    t.integer  "fee_id"
    t.integer  "product_group_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.index ["fee_id"], name: "index_product_group_fees_on_fee_id", using: :btree
    t.index ["product_group_id"], name: "index_product_group_fees_on_product_group_id", using: :btree
  end

  create_table "product_groups", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "business_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["business_id"], name: "index_product_groups_on_business_id", using: :btree
  end

  create_table "products", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.float    "price"
    t.boolean  "active"
    t.integer  "product_group_id"
    t.integer  "business_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.index ["business_id"], name: "index_products_on_business_id", using: :btree
    t.index ["product_group_id"], name: "index_products_on_product_group_id", using: :btree
  end

  create_table "tokens", force: :cascade do |t|
    t.string   "token"
    t.string   "token_type"
    t.string   "owner_type"
    t.integer  "owner_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["owner_type", "owner_id"], name: "index_tokens_on_owner_type_and_owner_id", using: :btree
  end

  create_table "user_customer_groups", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "customer_group_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.index ["customer_group_id"], name: "index_user_customer_groups_on_customer_group_id", using: :btree
    t.index ["user_id"], name: "index_user_customer_groups_on_user_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "avatar_url"
    t.integer  "active_refresh_token"
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.index ["active_refresh_token"], name: "index_users_on_active_refresh_token", unique: true, using: :btree
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

  create_table "value_types", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_foreign_key "business_business_statuses", "business_statuses"
  add_foreign_key "business_business_statuses", "businesses"
  add_foreign_key "fee_value_types", "fees"
  add_foreign_key "fee_value_types", "value_types"
  add_foreign_key "merchant_accounts", "businesses"
  add_foreign_key "order_item_product_attributes", "order_items"
  add_foreign_key "order_item_product_attributes", "product_attributes"
  add_foreign_key "order_item_products", "order_items"
  add_foreign_key "order_item_products", "products"
  add_foreign_key "order_items", "orders"
  add_foreign_key "order_order_statuses", "order_statuses"
  add_foreign_key "order_order_statuses", "orders"
  add_foreign_key "orders", "businesses"
  add_foreign_key "orders", "customer_groups"
  add_foreign_key "orders", "merchant_accounts"
  add_foreign_key "product_attributes", "attributes"
  add_foreign_key "product_attributes", "products"
  add_foreign_key "product_fees", "fees"
  add_foreign_key "product_fees", "products"
  add_foreign_key "product_group_fees", "fees"
  add_foreign_key "product_group_fees", "product_groups"
  add_foreign_key "product_groups", "businesses"
  add_foreign_key "products", "businesses"
  add_foreign_key "products", "product_groups"
  add_foreign_key "user_customer_groups", "customer_groups"
  add_foreign_key "user_customer_groups", "users"
end
