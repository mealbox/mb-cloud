class CreateFeeValueTypes < ActiveRecord::Migration[5.0]
  def change
    create_table :fee_value_types do |t|
      t.references :fee, foreign_key: true
      t.references :value_type, foreign_key: true

      t.timestamps
    end
  end
end
