class CreateOrderItems < ActiveRecord::Migration[5.0]
  def change
    create_table :order_items do |t|
      t.float :sub_total
      t.float :fee_total
      t.float :total
      t.integer :quantity
      t.text :notes
      t.references :order, foreign_key: true

      t.timestamps
    end
  end
end
