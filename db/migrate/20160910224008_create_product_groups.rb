class CreateProductGroups < ActiveRecord::Migration[5.0]
  def change
    create_table :product_groups do |t|
      t.string :name
      t.text :description
      t.references :business, foreign_key: true

      t.timestamps
    end
  end
end
