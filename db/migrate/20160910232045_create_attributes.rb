class CreateAttributes < ActiveRecord::Migration[5.0]
  def change
    create_table :attributes do |t|
      t.string :name
      t.text :description
      t.boolean :allow_multiple
      t.boolean :required

      t.timestamps
    end
  end
end
