class CreateProductFees < ActiveRecord::Migration[5.0]
  def change
    create_table :product_fees do |t|
      t.references :fee, foreign_key: true
      t.references :product, foreign_key: true

      t.timestamps
    end
  end
end
