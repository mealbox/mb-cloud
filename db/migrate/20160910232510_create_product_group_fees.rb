class CreateProductGroupFees < ActiveRecord::Migration[5.0]
  def change
    create_table :product_group_fees do |t|
      t.references :fee, foreign_key: true
      t.references :product_group, foreign_key: true

      t.timestamps
    end
  end
end
