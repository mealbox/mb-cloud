class CreateBusinesses < ActiveRecord::Migration[5.0]
  def change
    create_table :businesses do |t|
      t.string :name
      t.string :username
      t.text :description
      t.boolean :active

      t.timestamps
    end

    add_index :businesses, :username
  end
end
