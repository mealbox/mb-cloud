class CreateOrderItemProductAttributes < ActiveRecord::Migration[5.0]
  def change
    create_table :order_item_product_attributes do |t|
      t.references :order_item, foreign_key: true
      t.references :product_attribute, foreign_key: true

      t.timestamps
    end
  end
end
