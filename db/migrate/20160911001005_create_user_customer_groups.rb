class CreateUserCustomerGroups < ActiveRecord::Migration[5.0]
  def change
    create_table :user_customer_groups do |t|
      t.references :user, foreign_key: true
      t.references :customer_group, foreign_key: true

      t.timestamps
    end
  end
end
