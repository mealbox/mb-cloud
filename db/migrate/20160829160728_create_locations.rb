class CreateLocations < ActiveRecord::Migration[5.0]
  def change
    create_table :locations do |t|
      t.string :name
      t.string :country
      t.string :province
      t.string :city
      t.string :address_line_one
      t.string :address_line_two
      t.string :zip_code
      t.string :formatted_address
      t.float :lng
      t.float :lat

      t.references :owner, index: true, polymorphic: true

      t.timestamps
    end

  end
end
