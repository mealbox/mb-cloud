class CreateFees < ActiveRecord::Migration[5.0]
  def change
    create_table :fees do |t|
      t.string :name
      t.text :description
      t.float :value
      t.boolean :active

      t.timestamps
    end
  end
end
