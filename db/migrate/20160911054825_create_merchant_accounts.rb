class CreateMerchantAccounts < ActiveRecord::Migration[5.0]
  def change
    create_table :merchant_accounts do |t|
      t.references :business, foreign_key: true
      t.string :currency_code
      t.string :country_code
      t.string :provider_user_id
      t.string :provider_publishable_key
      t.string :provider_secret_key
      t.string :provider_access_token
      t.string :provider_refresh_token

      t.timestamps
    end
  end
end
