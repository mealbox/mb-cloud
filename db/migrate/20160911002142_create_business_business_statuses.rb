class CreateBusinessBusinessStatuses < ActiveRecord::Migration[5.0]
  def change
    create_table :business_business_statuses do |t|
      t.references :business, foreign_key: true
      t.references :business_status, foreign_key: true

      t.timestamps
    end
  end
end
