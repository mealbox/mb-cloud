class CreateProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :products do |t|
      t.string :name
      t.text :description
      t.float :price
      t.boolean :active
      t.references :product_group, foreign_key: true
      t.references :business, foreign_key: true

      t.timestamps
    end
  end
end
