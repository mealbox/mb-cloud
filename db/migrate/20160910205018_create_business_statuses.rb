class CreateBusinessStatuses < ActiveRecord::Migration[5.0]
  def change
    create_table :business_statuses do |t|
      t.string :name
      t.boolean :business_active
      t.text :description

      t.timestamps
    end
  end
end
