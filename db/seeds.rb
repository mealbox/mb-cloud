

#####################
# Statuses
#####################

  ## Business Statuses

    puts "####################################"
    puts "# Creating Business Statuses"
    puts "#"

      bs1 = BusinessStatus.create!({
        name: 'unverified',
        business_active: false
      })
      puts "# created #{ bs1.id} - #{bs1.name}"

      bs2 = BusinessStatus.create!({
        name: 'verified',
        business_active: true
      })
      puts "# created #{ bs2.id} - #{bs2.name}"

      bs3 = BusinessStatus.create!({
        name: 'disabled',
        business_active: false
      })
      puts "# created #{ bs3.id} - #{bs3.name}"

      bs4 = BusinessStatus.create!({
        name: 'archived',
        business_active: false
      })
      puts "# created #{ bs4.id} - #{bs4.name}"

    puts "####################################"


    puts " "
    puts " "
    puts " "


  ## OrderStatuses


    puts "####################################"
    puts "# Creating Order Statuses"
    puts "#"

    os1 = OrderStatus.create!({
      name: "Received By Mealbox",
      description: "The order has been created in the Mealbox system."
    })
    puts "# created #{ os1.id } - #{ os1.name }"

    os2 = OrderStatus.create!({
      name: "Delivered To Business",
      description: "The order has been successfully delivered to the Business' system."
    })
    puts "# created #{ os2.id } - #{ os2.name }"

    os3 = OrderStatus.create!({
      name: "In Progress",
      description: "The order has been moved to In Progress by the Business."
    })
    puts "# created #{ os3.id } - #{ os3.name }"

    os4 = OrderStatus.create!({
      name: "Awaiting Pickup",
      description: "The order has been completed by the business and is awaiting pickup."
    })
    puts "# created #{ os4.id } - #{ os4.name }"

    os8 = OrderStatus.create!({
      name: "Successfully Picked Up",
      description: "The order has been picked up successfully."
    })
    puts "# created #{ os8.id } - #{ os8.name }"

    os5 = OrderStatus.create!({
      name: "Awaiting Delivery",
      description: "The order has been completed by the business and is awaiting delivery."
    })
    puts "# created #{ os5.id } - #{ os5.name }"

    os6 = OrderStatus.create!({
      name: "Delivery Enroute",
      description: "The order is enroute by delivery."
    })
    puts "# created #{ os6.id } - #{ os6.name }"

    os7 = OrderStatus.create!({
      name: "Successfully Delivered",
      description: "The order has been delivered successfully."
    })
    puts "# created #{ os7.id } - #{ os7.name }"

    puts "####################################"


    puts " "
    puts " "
    puts " "


#####################
# Types
#####################

  ## ValueTypes

    puts "####################################"
    puts "# Creating Value Types"
    puts "#"

    vt1 = ValueType.create!({
      name: "Fixed",
      description: "Fixed value."
    })
    puts "# created #{ vt1.id } - #{ vt1.name }"
    vt2 = ValueType.create!({
      name: "Percent",
      description: "Percent-based value."
    })
    puts "# created #{ vt2.id } - #{ vt2.name }"

    puts "####################################"


    puts " "
    puts " "
    puts " "


#####################
# Finsihed
#####################

  puts "#### FINISHED ####"
