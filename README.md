# mb-cloud


## To Do

  - [ ] Setup Pundit / Rolify
  - [ ] Hours
  - [ ] Transactions ( Update OrderStatuses )
  - [ ] Providers (Stripe, Braintree, WePay)
  - [ ] OrdersController
  - [ ] UsersController
  - [ ] MealsController
  - [ ] Coupons?
  - [ ] Integrate Stripe
  - [ ] Users / CustomerGroups / PaymentOptions
  - [ ] Preferences / Allergies



  Rename Attribute to ProductAttributeGroup?




<!-- Product ->> Ingredients
ProductAttribute ->> Ingredients
Ingredient ->> Allergens ~# IngredientAllergen

User ->> Allergies ~# UserAllergy
Allergy -> Allergen

User ->> Preferences


DietaryPreferences
- ingredient
- value ( 0-1 )
- allergy? ( should we display a notice to the business ) -->

Product
|-> has_many :dietary_considerations, through: ProductDietaryConsiderations

ProductDietaryConsiderations
|-> has_one :dietary_consideration
|-> has_one :product

|-> value:boolean

DietaryConsiderations
|-> name:string
|-> description:text

UserDietaryConsiderations
|-> has_one :dietary_consideration
|-> has_one :user
|-> value:boolean
