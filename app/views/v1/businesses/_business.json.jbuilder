json.extract! business,
  :id,
  :name,
  :username,
  :description,
  :status,
  :active,
  :created_at,
  :updated_at

json.locations business.locations,
  partial: 'v1/locations/location',
  as: :location

json.phone_numbers business.phone_numbers,
  partial: 'v1/phone_numbers/phone_number',
  as: :phone_number

json.url v1_business_url(business)
