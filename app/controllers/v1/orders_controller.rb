class V1::OrdersController < V1::ApplicationController
  before_action :set_order, only: [:show, :update]

  def show
  end

  def create
    @order = Order.new(order_params)

    if @order.save
      render :show, status: :created, order: @order
    else
      render json: @order.errors, status: :unprocessable_entity
    end
  end

  def update
    if @order.update(order_params)
      render :show, status: :ok, order: @order
    else
      render json: @order.errors, status: :unprocessable_entity
    end
  end

  protected

  def set_order
    @order = Order.find(params[:id])
    # NOTE: this will probably be handled by Pundit
    # if @current_user.customer_groups_ids.include?(order.customer_group.id)
    #   @order = order
    # else
    #   render json: { errors: ['Not Authorized', 'Not Authorized for this resource'] }, status: :unauthorized
    # end
  end

  def order_params
    params.permit(
      :sub_total,
      :fee_total,
      :total,
      :notes,
      :currency_code,
      :customer_group_id,
      :business_id,
      :order_status,
      order_item_attributes: [
        :sub_total,
        :fee_total,
        :total,
        :quantity,
        :notes,
        :product_id,
        :product_attributes_ids
      ]
    )
end
