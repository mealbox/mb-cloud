class V1::BusinessesController < V1::ApplicationController
  before_action :set_business, only: [:show, :update, :disable]

  def show
  end

  def create
    @business = Business.new(business_params)

    if @business.save
      render :show, status: :created, business: @business
    else
      render json: @business.errors, status: :unprocessable_entity
    end
  end

  def update
    if @business.update(business_params)
      render :show, status: :ok, business: @business
    else
      render json: @business.errors, status: :unprocessable_entity
    end
  end

  protected

  def set_business
    business = Business.find(params[:id])

    if business.active? && business.status.business_active?
      @business = business
      return @business
    else
      render json: { errors: ['Not Found'] }, status: 404
    end
  end

  def business_params
    params.permit(
      :name,
      :username,
      :description,
      :active,
      :status,
      phone_numbers_attributes: [
        :name,
        :value,
      ],
      locations_attributes: [
        :name,
        :address_line_one,
        :address_line_two,
        :city,
        :province,
        :country,
        :zip_code,
        :formatted_address,
        :lat,
        :lng
      ]
    )
  end


end
