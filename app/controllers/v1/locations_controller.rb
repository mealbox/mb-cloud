class V1::LocationsController < V1::ApplicationController
  before_action :set_location, only: [:update, :destroy]

  def update
    if @location.update(location_params)
      render :show, status: :ok, location: @location
    else
      render json: @location.errors, status: :unprocessable_entity
    end
  end

  def destroy
    if @location.destroy
      render json: { result: [ 'Location deleted' ] }, status: 200
    else
      render json: @location.errors, status: :unprocessable_entity
    end
  end

  private
    def set_location
      @location = Location.find(params[:id])
    end

    def location_params
      params.permit(
        :name,
        :address_line_one,
        :address_line_two,
        :city,
        :province,
        :country,
        :zip_code,
        :formatted_address,
        :lat,
        :lng,
      )
    end
end
