class V1::ApplicationController < ActionController::API
  respond_to :json
  before_action :authenticate_request!
  skip_before_action :authenticate_request!, only: [:refresh]

  def test
    render json: { message: "Yay!" }
  end

  def refresh
    refresh_token = JsonWebToken.decode( params['refresh_token'] )

    if refresh_token
      token = Token.where(token: refresh_token["token"]).first

      if token
        user = User.where(active_refresh_token: token.id).first

        if user
          @current_user = user
          render "v1/users/auth_token"
        else
          render json: { errors: ['Invalid Email or Password'] }, status: :unauthorized
        end
      else
        render json: { errors: ['Invalid User Refresh Token'] }, status: :unauthorized
      end
    else
      render json: { errors: ['Invalid Refresh Token'] }, status: :unauthorized
    end
  end

  protected

  def authenticate_request!
    unless user_id_in_token?
      render json: { errors: ['Not Authenticated'] }, status: :unauthorized
      return
    end
    @current_user = User.find(auth_token[:user_id])
  rescue JWT::ExpiredSignature
    render json: { errors: ['Token Expired'] }, status: :unauthorized
  rescue JWT::VerificationError, JWT::DecodeError
    render json: { errors: ['Not Authenticated'] }, status: :unauthorized
  end

  private

  def http_token
    @http_token ||= if request.headers['Authorization'].present?
      request.headers['Authorization'].split(' ').last
    end
  end

  def auth_token
    @auth_token ||= JsonWebToken.decode(http_token)
  end

  def user_id_in_token?
    http_token && auth_token && auth_token[:user_id].to_i
  end
end
