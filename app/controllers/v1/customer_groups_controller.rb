class V1::CustomerGroups < V1::ApplicationController
  before_action :set_customer_group, only: [:show, :update]

  def show
  end

  def update

    if @customer_group.update(customer_group_params)
      render @show, status: :ok, customer_group: @customer_group
    else
      render json: @customer_group.errors, status: :unprocessable_entity
    end
  end

  protected

  def set_customer_group
    @customer_group = CustomerGroup.find(params[:id])
  end

  def customer_group_params
    params.permit(
      :name,
      :description
    )
  end
end
