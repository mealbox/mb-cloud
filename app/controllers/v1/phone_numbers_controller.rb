class V1::PhoneNumbersController < V1::ApplicationController
  before_action :set_phone_number, only: [:update, :destroy]

  def create
    @phone_number = PhoneNumber.new(phone_number_params)

    if @phone_number.save
      render :show, status: :ok, phone_number: @phone_number
    else
      render json: @phone_number.errors, status: :unprocessable_entity
    end
  end

  def update
    if @phone_number.update(phone_number_params)
      render :show, status: :ok, phone_number: @phone_number
    else
      render json: @phone_number.errors, status: :unprocessable_entity
    end
  end

  def destroy
    if @phone_number.destroy
      render json: { result: [ 'Phone Number deleted' ] }, status: 200
    else
      render json: @phone_number.errors, status: :unprocessable_entity
    end
  end

  private
    def set_phone_number
      @phone_number = PhoneNumber.find(params[:id])
    end

    def phone_number_params
      params.permit(
        :name,
        :value
      )
    end
end
