class Fee < ApplicationRecord
  has_one :value_type,         through: :fee_value_types
  has_one :fee_value_type
  has_many :product_groups,    through: :product_group_fees
  has_many :products,          through: :product_fees
  has_many :product_group_fees
  has_many :product_fees

  validates :name,  presence: true
  validates :value, presence: true
end
