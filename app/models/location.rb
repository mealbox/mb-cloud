class Location < ApplicationRecord
  after_create :generate_formatted_address

  # geocoded_by :formatted_address,
  #   :latitude => :lat, :longitude => :lng
  # after_validation :geocode

  belongs_to :owner, polymorphic: true

  validates :name,             presence: true
  validates :address_line_one, presence: true
  validates :city,             presence: true
  validates :province,         presence: true
  validates :country,          presence: true
  validates :zip_code,         presence: true

  def generate_formatted_address
    self.formatted_address = self.address_line_one.to_s  + ", " +
                             self.address_line_two ? self.address_line_two.to_s  + ", " : "" +
                             self.city.to_s              + ", " +
                             self.province.to_s          + ", " +
                             self.country.to_s           + " "  +
                             self.zip_code.to_s
    self.save
  end
end
