class BusinessBusinessStatus < ApplicationRecord
  belongs_to :business
  belongs_to :business_status
end
