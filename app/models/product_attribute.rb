class ProductAttribute < ApplicationRecord
  belongs_to :product
  belongs_to :attribute

  validates :name,         presence: true
  validates :price,        presence: true
end
