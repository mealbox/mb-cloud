class CustomerGroup < ApplicationRecord
  # after_create :set_defaults

  has_many :users, through: :user_customer_groups
  has_many :user_customer_groups
  has_many :orders

  # private
  #
  # def set_defaults
  #   unless self.name
  #     user_names = []
  # Find all the user's names and make that the title?
  # If it's just one user's make it their name?
  # end
end
