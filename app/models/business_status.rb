class BusinessStatus < ApplicationRecord
  has_many :businesses, through: :business_business_statuses
  has_many :business_business_statuses

  validates :name,
    uniqueness:  true,
    presence:    true,
    allow_blank: false
end
