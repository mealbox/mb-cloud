class MerchantAccount < ApplicationRecord
  belongs_to :business

  validates :currency_code, presence: true
  validates :country_code,  presence: true
  validates :provider_user_id,
    presence:   true,
    uniqueness: true
  validates :provider_publishable_key,
    presence:   true,
    uniqueness: true
  validates :provider_secret_key,
    presence:   true,
    uniqueness: true
  validates :provider_access_token,
    presence:   true,
    uniqueness: true
  validates :provider_refresh_token,
    presence:   true,
    uniqueness: true
end
