class Order < ApplicationRecord
  belongs_to :customer_group
  belongs_to :business
  belongs_to :merchant_account
  has_many :order_statuses, through: :order_order_statuses
  has_many :order_order_statuses
  has_many :order_items, dependent: :destroy
  has_one :location,
    dependent:  :destroy,
    as:         :owner,
    inverse_of: :owner

  validates :sub_total,     presence: true
  validates :fee_total,     presence: true
  validates :total,         presence: true
  validates :currency_code, presence: true

  accepts_nested_attributes_for :order_items
  accepts_nested_attributes_for :location

  def status
    self.order_statuses.last
  end
end
