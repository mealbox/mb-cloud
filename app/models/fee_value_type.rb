class FeeValueType < ApplicationRecord
  belongs_to :fee
  belongs_to :value_type
end
