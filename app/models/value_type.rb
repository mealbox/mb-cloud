class ValueType < ApplicationRecord
  has_many :fees, through: :fee_value_types

  validates :name,        presence: true
  validates :description, presence: true
end
