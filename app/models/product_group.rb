class ProductGroup < ApplicationRecord
  belongs_to :business

  has_many :products
  has_many :product_group_fees
  has_many :fees, through: :product_group_fees

  validates :name,        presence: true
end
