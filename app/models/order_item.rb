class OrderItem < ApplicationRecord
  belongs_to :order
  has_many :product_attributes, through: :order_item_product_attributes
  has_many :order_item_product_attributes
  has_one :product, through: :order_item_product
  has_one :order_item_producto

  validates :sub_total, presence: true
  validates :fee_total, presence: true
  validates :total,     presence: true
end
