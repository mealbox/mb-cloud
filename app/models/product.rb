class Product < ApplicationRecord
  belongs_to :product_group
  belongs_to :business

  has_many :fees, through: :product_fees
  has_many :product_fees
  has_many :product_attributes

  validates :name,  presence: true
  validates :price, presence: true
end
