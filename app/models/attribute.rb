class Attribute < ApplicationRecord
  has_many :product_attributes

  validates :name, presence: true
end
