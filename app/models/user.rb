class User < ApplicationRecord
  after_create :create_customer_group

  has_many :tokens,
    dependent:  :destroy,
    as:         :owner,
    inverse_of: :owner

  has_many :phone_numbers,
    dependent:  :destroy,
    as:         :owner,
    inverse_of: :owner

  has_many :customer_groups, through: :user_customer_groups
  has_many :user_customer_groups

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
         #:confirmable

  validates :first_name, presence: true
  validates :last_name,  presence: true

  def name
    return self.first_name + " " + self.last_name
  end

  def orders
    orders = []
    self.customer_groups.each do |cg|
      cg.orders.each do |o|
        orders << o
      end
    end

    return orders
  end

  def customer_groups_ids
    customer_groups_ids = []

    self.customer_groups.each do |cg|
      customer_groups_ids << cg.id
    end

    return customer_groups_ids
  end

  def find_or_create_refresh_token
    if !self.active_refresh_token
      self.generate_refresh_token
    end

    return self.refresh_token
  end

  def generate_refresh_token
    token_id = Token.generate_user_token( "refresh", self )
    self.active_refresh_token = token_id
    self.save
  end

  def refresh_token
    return Token.find( self.active_refresh_token )
  end

  private

  def create_customer_group
    self.customer_groups.create( name: self.name )
  end
end
