class Business < ApplicationRecord
  after_create :set_defaults

  has_many :business_statuses, through: :business_business_statuses
  has_many :business_business_statuses
  has_many :products
  has_many :product_groups
  has_many :merchant_accounts
  has_many :locations,
    dependent:  :destroy,
    as:         :owner,
    inverse_of: :owner
  has_many :phone_numbers,
    dependent:  :destroy,
    as:         :owner,
    inverse_of: :owner

  accepts_nested_attributes_for :locations,     allow_destroy: true
  accepts_nested_attributes_for :phone_numbers, allow_destroy: true

  validates :name, presence: true
  validates :username,
    presence:   true,
    uniqueness: true

  def status
    self.business_statuses.last
  end

  private

  def set_defaults
    self.active = true
    BusinessBusinessStatus.create(
      business_status_id: 2, ## active
      business_id: self.id
    )
    self.save!
  end
end
