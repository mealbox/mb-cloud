class OrderItemProductAttribute < ApplicationRecord
  belongs_to :order_item
  belongs_to :product_attribute
end
