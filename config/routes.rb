Rails.application.routes.draw do

  ####################################
  # Version 1.x.x API


  namespace :v1, defaults: { format: 'json' } do

    ## Authentication
      devise_for :users,
        path_names: {
          sign_in: 'sessions',
          sign_out: 'sessions'
        },
        controllers: {
          sessions: 'v1/users/sessions',
          registrations: 'v1/users/registrations'
        }
      post 'users/sessions/refresh', to: 'application#refresh', as: 'refresh_user_session'

    ## CustomerGroups
      resources :customer_groups, only: [:index, :show] do
        # resources :payment_cards
      end

    ## Orders
      resources :orders, only: [:create, :update, :show]

    ## Businesses
      resources :businesses do
        resources :phone_numbers
        resources :locations
      end

    ## Test
      get 'test', to: 'application#test'


  end

  # END 1.x.x
  ####################################


  # get '/', to: redirect("http://mealbox.ca")
end
