BusinesBusinesStatus
business:references
business_status:references

business
has_many :products
has_many :merchant_accounts

Fees
name:string description:text value:float value_type:integer:index add:boolean active:boolean

ValueTypes
name:string
description:text

FeeValueTypes fee:references value_type:references

name:string description:text price:float active:boolean product_group:references business:references

ProductGroup
name:string description:text business:references

ProductAttribute name:string description:text price:float active:boolean product:references attribute:references

ProductVariant
product:references
active:boolean
price:float

rails g model ProductVariantFee fee:references product_variant:references

UserCustomerGroup
user:references
customer_group:references

CustomerGroup
name:string
description:text

has_many :users, through: :user_customer_groups

Order
sub_total:float
fee_total:float
total:float
notes:text
currency_code:string
location:references
customer_group:references
business:references
merchant_account:references

OrderOrderStatus
order:references
order_status:references

OrderStatus
name:string
description:string

OrderItem
sub_total:float
fee_total:float
total:float
quantity:integer
notes:text
order:references

OrderItemProduct
order_item:references
product:references

OrderItemProductAttribute
order_item:references
product_attribute:references

MerchantAccount
business:references
currency_code:string
country_code:string
provider_user_id:string
provider_publishable_key:string
provider_secret_key:string
provider_access_token:string
provider_refresh_token:string



Transactions
business:references
merchant_account:references
customer_group:references
order:references
description:text
total:float
fee:float
receipt_email:string
receipt_number:string
failure_code:string
failure_message:string
statement_descriptor:text
date_charge_expected:datetime
date_charged:datetime


TransactionStatus
name:string
description:text

TransactionTransactionStatus
transaction:references
transaction_status:references
