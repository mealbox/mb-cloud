require 'test_helper'

class LocationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @location = locations(:one)
  end

  test "should get index" do
    get locations_url, as: :json
    assert_response :success
  end

  test "should create location" do
    assert_difference('Location.count') do
      post locations_url, params: { location: { city_long: @location.city_long, city_short: @location.city_short, country_long: @location.country_long, country_short: @location.country_short, formatted_address: @location.formatted_address, lat: @location.lat, lng: @location.lng, name: @location.name, province_long: @location.province_long, province_short: @location.province_short, street_name_long: @location.street_name_long, street_name_short: @location.street_name_short, street_number: @location.street_number, unit_name: @location.unit_name, unit_number: @location.unit_number } }, as: :json
    end

    assert_response 201
  end

  test "should show location" do
    get location_url(@location), as: :json
    assert_response :success
  end

  test "should update location" do
    patch location_url(@location), params: { location: { city_long: @location.city_long, city_short: @location.city_short, country_long: @location.country_long, country_short: @location.country_short, formatted_address: @location.formatted_address, lat: @location.lat, lng: @location.lng, name: @location.name, province_long: @location.province_long, province_short: @location.province_short, street_name_long: @location.street_name_long, street_name_short: @location.street_name_short, street_number: @location.street_number, unit_name: @location.unit_name, unit_number: @location.unit_number } }, as: :json
    assert_response 200
  end

  test "should destroy location" do
    assert_difference('Location.count', -1) do
      delete location_url(@location), as: :json
    end

    assert_response 204
  end
end
